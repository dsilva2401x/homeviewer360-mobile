(function (ang) {

	var app = ang.module('app', []);

	app.directive('eqviewer', function () {
		return {
			restrict: 'EA',
			link: function (scope, elem, attrs) {

				// ---
				var camera = new THREE.PerspectiveCamera( 70, elem[0].clientWidth / elem[0].clientHeight, 1, 1100 );
				camera.target = new THREE.Vector3( 0, 0, 0 );
				var scene = new THREE.Scene();

				// ---
				var mesh = new THREE.Mesh();
				mesh.geometry = new THREE.SphereGeometry( 500, 60, 40 );
				mesh.material = new THREE.MeshBasicMaterial({
					map: THREE.ImageUtils.loadTexture( 'demo.jpg')
					// color: 0x333333
				});
				mesh.scale.x = -1;

				// ---
				scene.add( mesh );

				// ---
				var renderer = new THREE.CanvasRenderer();
				renderer.setSize( elem[0].clientWidth, elem[0].clientHeight );

				// ---
				elem[0].appendChild( renderer.domElement );

				// ---
				renderer.render( scene, camera );

				setInterval(function () {
					renderer.render( scene, camera );
					console.log(123)
				},1000);

			}
		}
	});

})(angular)