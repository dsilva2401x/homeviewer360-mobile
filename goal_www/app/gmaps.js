(function(ang) {

	var module = ang.module('eqTour');

	module.directive('gmaps', function() {
		return {
			restrict: 'EA',
			link: function( scope, elem, attrs ) {
				var mapOptions = {
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(elem[0],mapOptions);
				var myPos = new google.maps.LatLng(
					scope.models.building.location.latitude,
					scope.models.building.location.longitude
				);
				map.setCenter(myPos);
				var markerPosition = new google.maps.LatLng(
					scope.models.building.location.latitude,
					scope.models.building.location.longitude
				);
				var marker = new google.maps.Marker({
					position: markerPosition,
					map: map
				});
				marker.setMap(map);
			}
		}
	});

})(angular)