(function (ang) {

	// id: 7

    var app = ang.module('eqTour', ['eqViewer', 'resources', 'ng-socket-io']);
    var envProd = 1;

    app.config( ['$resourcesProvider', '$socketProvider', function ($resourcesProvider, $socketProvider) {

	    $resourcesProvider.init({
	        resources: {
	            View: {
	                route: (
	                	envProd ?
	                	'http://api.homeviewer360.com/api/v1/vistas/:viewId/' : 
	                	'demos/view-:viewId.json'
	                )
	            },
	            Building: {
	                route: (
	                	envProd ?
	                	'http://api.homeviewer360.com/api/v1/inmuebles/:buildingId/' :
	                	'demos/building.json'
	                )
	            }
	        }
	    });

	}]);

    app.filter('buildingFilter', function () {
		return function (buildingData) {
			var bData = {};
			bData.id = buildingData.id;
			bData.location = { latitude:buildingData.latitud,longitude:buildingData.longitud };
			bData.views = [];
			buildingData.vistas.forEach(function (v) {
				bData.views.push({
					id: v.id,
					name: v.nombre,
					imgSrc: v.imagen
				});
			});
			return bData;
		}
	});

	app.filter('viewFilter', function () {
		return function (viewData) {
			var vData = {};
			vData.id = viewData.id;
			vData.name = viewData.nombre;
			vData.light = viewData.luminosidad;
			vData.imgSrc = viewData.imagen;
			vData.markers = [];
			vData.adjViews = [];
			viewData.conexiones_origen.forEach(function (av) {
				av.vista_destino = av.vista_destino || {};
				vData.adjViews.push({
					id: av.vista_destino.id,
					imgSrc: av.vista_destino.imagen,
					xDir: {
						fromCurrent: av.x_origen,
						fromTarget: av.x_destino
					},
					yDir: {
						fromCurrent: av.y_origen,
						fromTarget: av.y_destino
					}
				})
			});
			return vData;
		}
	});

	app.controller('eqController', function ($scope, $interval, $window, $resources, $timeout, $filter) {
		$scope.methods = $scope.methods || {};
		$scope.models = $scope.models || {};

		// Methods
			$scope.methods.focusTarget = function (alpha, beta, callback) {
				var zoom = 30;
				var counter = 0;
				var intervalTime = 10;
				var duration = 2000;
				var nSteps = duration/intervalTime;
				var zoomDist = zoom - $scope.models.fZoom;
				var betaDist = beta - $scope.models.fBeta;
				var alphaDist = alpha - $scope.models.fAlpha;
				if ( Math.abs(alphaDist)>180 ) {
					if (alphaDist<0) alphaDist = 360+alphaDist;
					else alphaDist = alphaDist-360;
				}
				var betaStep = betaDist/nSteps;
				var alphaStep = alphaDist/nSteps;
				var zoomStep = zoomDist/nSteps;
				var intervalPromise = $interval(function () {
					$scope.models.fAlpha += alphaStep;
					$scope.models.fBeta += betaStep;
					$scope.models.fZoom += zoomStep;
					counter += intervalTime;
					if ( counter >= duration ) {
						if (callback) callback();
						$interval.cancel( intervalPromise );
					}
				},intervalTime);
			}
			$scope.methods.getUrlParams = function () {
				var p = {};
				var params = $window.location.search;
				params = params.substring( 1, params.length - (params[params.length-1]=='/') );
				params.split('&').forEach(function (_p) {
					p[ _p.split('=')[0] ] = _p.split('=')[1];
				});
				return p;
			}
			$scope.methods.loadBuilding = function () {
				var bId = $scope.methods.getUrlParams().buildingId;
				if (!bId) {
					document.body.innerHTML = 'Invalid building id, pass it like this: [url..]?buildingId=123';
					return;
				}
				$resources.Building.get({
					urlParams: { buildingId: bId }
				}).then(function (resp) {
					var building = resp.data;
					
					// Production
						if (envProd) building = $filter('buildingFilter')(building);

					$scope.models.building = building;
					$scope.methods.loadView( $scope.models.building.views[0].id );
				}).catch(function (err) {
					console.warn('Error loading building '+bId, err);
				})
			}
			$scope.methods.loadView = function (vId, callback) {
				$resources.View.get({
					urlParams: { viewId: vId }
				}).then(function (resp) {
					var view = resp.data;
					console.log( 'Loaded view: ',view )
					// Production
						if (envProd) view = $filter('viewFilter')(view);
					
					$scope.models.fZoom = 70;
					$scope.models.currentView = view;
					console.log( $scope.models.currentView );
					if (callback) callback();
				}).catch(function (err) {
					console.warn('Error loading view '+vId, err);
				})
			}
			$scope.methods.changeView = function (v) {
				// Update background eqviewer
				$scope.models.changingViewStatus = true;
				$scope.models.bImg = v.imgSrc;
				$scope.models.bAlpha = (v.xDir.fromTarget+180)%360;
				
				// BETA
				v.yDir = v.yDir || { fromTarget: 0 };
				$scope.models.bBeta = v.yDir.fromTarget;
				
				// Focus target
				$scope.methods.focusTarget(v.xDir.fromCurrent, v.yDir.fromTarget, function () {
					// Hide front eqviewer
					$scope.models.fOpacity = 0;
					// Reset to front view
					$timeout(function () {
						$scope.methods.loadView(v.id);
						$scope.models.fAlpha = (v.xDir.fromTarget+180)%360;
						$scope.models.fBeta = v.yDir.fromTarget;
						$timeout(function () {
							$scope.models.fOpacity = 1;
						},1000);
						$scope.models.changingViewStatus = false;
					},1000);
				});
				// console.log( v );
			}			

		// Models
			$scope.models.fOpacity = 1;
			$scope.models.fAlpha = 0;
			$scope.models.fBeta = 0;
			$scope.models.bAlpha = 0;
			$scope.models.bBeta = 0;
			$scope.models.bImg = null;
			$scope.models.building = {};
			$scope.models.currentView = {};
			$scope.models.currentContainer = 'views';
			$scope.models.viewsContainerStatus = true;
			$scope.Math = Math;
			$scope.DEG2RAD = Math.PI/180.0;
			$scope.targetsContainerLarge = Math.round(document.body.offsetWidth*0.8);


		// Init
			$scope.methods.loadBuilding();

	});

	// Targets container settings: (Improve)
		var targetsContainer = document.getElementById('targets-container');
		var tcLarge = Math.round(document.body.offsetWidth*0.8);
		console.log( targetsContainer.style	, tcLarge )
		targetsContainer.style.width = tcLarge+'px';
		targetsContainer.style.height = tcLarge+'px';
		targetsContainer.style.left = (document.body.offsetWidth*0.5-tcLarge*0.5) + 'px';
		targetsContainer.style.bottom = (0-tcLarge*0.5) + 'px';



})(angular);