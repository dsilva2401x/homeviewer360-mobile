<html>

    <head>
        <link rel="stylesheet" type="text/css" href="app/main.css">
    </head>

    <body ng-app="eqTour"  ng-controller="eqController">

        <div id="eq" class="main-container">

            <!-- TOUR -->
                <div class="cover" style="background: #000;"></div>
                <div class="cover" style="opacity: {{ models.light }}; cursor: url('res/pointer.png'), default;">
                    <eq-viewer
                        class="cover soft-transition"
                        alpha="models.bAlpha"
                        beta="models.bBeta"
                        img="models.bImg"
                    ></eq-viewer>
                    <eq-viewer
                        style="opacity: {{ models.fOpacity }};"
                        class="cover soft-transition"
                        alpha="models.fAlpha"
                        beta="models.fBeta"
                        zoom="models.fZoom"
                        img="models.currentView.imgSrc"
                    >
                        <!-- <eq-marker
                            distance="10"
                            ng-repeat="adj in models.currentView.adjViews"
                            alpha="adj.xDir.fromCurrent"
                            beta="adj.yDir.fromCurrent*-1"
                            ng-click="methods.changeView(adj);"
                        ></eq-marker> -->
                    </eq-viewer>
                    <div id="targets-container">
                        <div
                            class="cover"
                            style="
                                -moz-transform: rotateZ( {{ -models.fAlpha-90 }}deg );
                                -webkit-transform: rotateZ( {{ -models.fAlpha-90 }}deg );
                                transform: rotateZ( {{ -models.fAlpha-90 }}deg );
                            "
                        >
                            <img
                                ng-repeat="adj in models.currentView.adjViews"
                                src="res/arrow-target.png"
                                width="80"
                                ng-click="methods.changeView(adj);"
                                style="
                                    position: absolute;
                                    left: {{ Math.round( targetsContainerLarge/2*( 1 + Math.cos(adj.xDir.fromCurrent*DEG2RAD) ) ) - 40 }}px;
                                    top: {{ Math.round( targetsContainerLarge/2*( 1 + Math.sin(adj.xDir.fromCurrent*DEG2RAD) ) ) - 40 }}px;
                                    -webkit-border-radius: 100%';
                                    -moz-border-radius: 100%;
                                    border-radius: 100%;
                                    -moz-transform: rotateZ( {{ adj.xDir.fromCurrent+180 }}deg );
                                    -webkit-transform: rotateZ( {{ adj.xDir.fromCurrent+180 }}deg );
                                    transform: rotateZ( {{ adj.xDir.fromCurrent+180 }}deg );
                                    box-shadow: 0px 0px 30px #555;
                                    cursor: pointer;
                                "
                            >
                        </div>
                    </div>
                </div>

            <!-- Options -->
                <div class="light">
                    <img
                        style="
                            right: 100px;
                            position:absolute;
                        "
                        width="35"
                        src="res/brillo.png"
                    >
                    <input
                        style="
                            height:10px;
                            position:absolute;
                            top:12px;
                            right:0px;
                            width:90px;
                        "
                        ng-model="models.light"
                        min="0.3"
                        max="1"
                        step="0.01"
                        type="range"
                    >
                </div>
                <div class="temperature">
                    <img width="35" src="res/temperatura.png">
                    <div
                        style="
                            position: absolute;
                            top: 50px;
                            right: 11px;
                            width: 10px;
                            height: 80px;
                            border: 1px solid #fff;
                            -webkit-border-radius: 4px;
                            -moz-border-radius: 4px;
                            border-radius: 4px;
                        "   
                    >
                        <div
                            style="
                                position: absolute;
                                bottom: 0px;
                                right: 0px;
                                width: 100%;
                                height: 60%;
                                background: #fff;
                            "   
                        >
                        </div>
                    </div>
                </div>


            <!-- Map container -->
                <div gmaps ng-if="models.currentContainer == 'map'" class="cover"></div>
                <div sv ng-if="models.currentContainer == 'sv'" class="cover"></div>

            <!-- Options container -->
                <div
                    style="position: absolute; left: 0px; top: 0px; padding: 10px; ">
                    <button ng-click="models.currentContainer='views';" class="option-btn">
                        <img src="res/viewer.png" style="width: 100%;">
                    </button>
                    <button ng-click="models.currentContainer='map';"  class="option-btn">
                        <img src="res/marker.png" style="width: 100%;">
                    </button>
                    <button ng-click="models.currentContainer='sv';"  class="option-btn">
                        <img src="res/street.png" style="width: 100%;">
                    </button>
                </div>


        </div>

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="libs/three-58.min.js"></script>
        <script src="libs/threex.domevents.js"></script>
        <script src="libs/socket.io.js"></script>
        <script src="libs/angular.min.js"></script>
        <script src="libs/ng-socket-io.js"></script>
        <script src="libs/resources.min.js"></script>
        <script src="libs/eq-viewer.js"></script>
        <script src="app/eq-tour.js"></script>
        <script src="app/gmaps.js"></script>
        <script src="app/streetview.js"></script>
    </body>

</html>