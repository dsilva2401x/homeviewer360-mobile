(function(ang) {

	var app = ang.module('app');

	app.controller('buildingController', ['$scope', '$http', 'downloadViewData', function( $scope, $http, downloadViewData ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Methods
			$scope.methods.loadBuilding = function (bId) {
				$http.get('demos/buildings'+bId+'.json').then(function (resp) {
					$scope.models.building = resp.data;
					downloadViewData($scope.models.building.views[0].id, function (data) {
						$scope.models.equiTour.initView(data);
					});
				}).catch(function (err) {
					console.log( err );
				});

			}

		// Init
			$scope.methods.loadBuilding( 2 );


	}]);

})(angular)