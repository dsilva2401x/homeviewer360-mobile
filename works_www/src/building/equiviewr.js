var EquiViewr = function(config) {
	/*
		Params:
			config.container
			config.imgSrc
			config.limits
				minY
				maxY
	*/

	// Variables
		const DEG2RAD = Math.PI/180.0;
		var camera, scene, renderer, containerDims, imageSource,
		onPointerDownPointerX, onPointerDownPointerY,
		minY, maxY, container, mainContainer, equiContainer,
		mesh,
		targetContainer, targetContainerWidth, targetSubContainer,
		loaderImg,
		onPointerDownLon, onPointerDownLat;

		var fov = 70,
		texture_placeholder,
		isUserInteracting = false,
		lon = 0, onMouseDownLon = 0,
		lat = 0, onMouseDownLat = 0,
		phi = 0, theta = 0,
		self = this,
		eventRegisteredActions = {};
		minY = ( (config.limits&&config.limits.minY) || -90 );
		maxY = ( (config.limits&&config.limits.maxY) || 90 );
		imageSource = ( config.imgSrc || '' );
		mainContainer = config.container;
		containerDims = {
			width : mainContainer.clientWidth,
			height : mainContainer.clientHeight
		};

	// Methods

		var basicSetup = function() {

			// EquiContainer
				equiContainer = document.createElement('div');
				equiContainer.style['position'] = 'absolute';
				equiContainer.style['left'] = '0px'
				equiContainer.style['top'] = '0px';
				equiContainer.style['width'] = '100%';
				equiContainer.style['height'] = '100%';

			// Target container
				targetContainerWidth = Math.round(containerDims.width*0.8);
				targetContainer = document.createElement('div');
				targetContainer.style['position'] = 'absolute';
				targetContainer.style['left'] = Math.round(containerDims.width/2-targetContainerWidth/2)+'px';
				targetContainer.style['bottom'] = Math.round(targetContainerWidth/-2)+'px';
				targetContainer.style['width'] = targetContainerWidth+'px';
				targetContainer.style['height'] = targetContainerWidth+'px';
				targetContainer.style['-moz-transform'] = 'perspective( 900px ) rotateX( 70deg )';
				targetContainer.style['-webkit-transform'] = 'perspective( 900px ) rotateX( 70deg )';
				targetContainer.style['transform'] = 'perspective( 900px ) rotateX( 70deg )';

			// Target subcontainer
				targetSubContainer = document.createElement('div');
				targetSubContainer.style['position'] = 'absolute';
				targetSubContainer.style['left'] = '0px'
				targetSubContainer.style['top'] = '0px';
				targetSubContainer.style['width'] = '100%';
				targetSubContainer.style['height'] = '100%';
				targetSubContainer.style['-webkit-border-radius'] = '100%';
				targetSubContainer.style['-moz-border-radius'] = '100%';
				targetSubContainer.style['border-radius'] = '100%';
			
				self.on('move', updateTargetContainerRotation);
				mainContainer.addEventListener( 'mousedown', onContainerMouseDown, false );
				mainContainer.addEventListener( 'mousemove', onContainerMouseMove, false );
				mainContainer.addEventListener( 'mouseup', onContainerMouseUp, false );

				// HERE
				mainContainer.addEventListener( 'touchstart', function (ev) {
					ev.preventDefault();
					var touch = ev.touches[0];
					isUserInteracting = true;
					onPointerDownPointerX = ev.touches[0].clientX;
					onPointerDownPointerY = ev.touches[0].clientY;
					onPointerDownLon = lon;
					onPointerDownLat = lat;
				}, false );
				mainContainer.addEventListener( 'touchmove', function (ev) {
					ev.preventDefault();
					var touch = ev.touches[0];
					if ( isUserInteracting ) {
						lon = ( onPointerDownPointerX - touch.clientX ) * 0.1 + onPointerDownLon;
						lon = ( lon + 360 )%360;
						lat = ( touch.clientY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;
						lat = Math.min( maxY, Math.max( lat, minY) );
						// Trigger move event
						triggerEvent('move', function(action) {
							action({
								x: lon,
								y: lat
							});
						});
					}
				}, false );
				mainContainer.addEventListener( 'touchend', function (ev) {
					isUserInteracting = false;
				}, false )

				window.requestAnimFrame = (function(){
					return  window.requestAnimationFrame 		||
							window.webkitRequestAnimationFrame 	||
							window.mozRequestAnimationFrame 	||
							function( callback ){
								window.setTimeout(callback, 1000 / 60);
							};
				})();

				targetContainer.appendChild(targetSubContainer);
				mainContainer.appendChild(equiContainer);
				mainContainer.appendChild(targetContainer)
		}

		var setEquiviewImage = function(imgSrc) {
			camera = new THREE.PerspectiveCamera( fov, containerDims.width / containerDims.height, 1, 1100 );
			camera.target = new THREE.Vector3( 0, 0, 0 );
			scene = new THREE.Scene();
			
			/*mesh = new THREE.Mesh( 
				new THREE.SphereGeometry( 500, 60, 40 ),
				new THREE.MeshBasicMaterial({
					map: THREE.ImageUtils.loadTexture( imgSrc ) 
				})
			);*/
			mesh = new THREE.Mesh();
			mesh.geometry = new THREE.SphereGeometry( 500, 60, 40 );
			mesh.material = new THREE.MeshBasicMaterial({
				map: THREE.ImageUtils.loadTexture( imgSrc ) 
			})

			mesh.scale.x = -1;
			scene.add( mesh );
			// renderer = new THREE.WebGLRenderer();
			renderer = new THREE.CanvasRenderer();
			// renderer = new THREE.SVGRenderer();
			// renderer = new THREE.CSS3DRenderer();
			renderer.setSize( containerDims.width, containerDims.height );
			equiContainer.innerHTML = '';
			equiContainer.appendChild( renderer.domElement );
			// alert( equiContainer.innerHTML );
		}

		var updateTargetContainerRotation = function(data) {
			targetContainer.style['-moz-transform'] = 'perspective( 900px ) rotateX( '+( 70+data.y*0.4 )+'deg )';
			targetContainer.style['-webkit-transform'] = 'perspective( 900px ) rotateX( '+( 70+data.y*0.4 )+'deg )';
			targetContainer.style['transform'] = 'perspective( 900px ) rotateX( '+( 70+data.y*0.4 )+'deg )';
			targetSubContainer.style['-moz-transform'] = 'rotateZ( '+ -data.x +'deg )';
			targetSubContainer.style['-webkit-transform'] = 'rotateZ( '+ -data.x +'deg )';
			targetSubContainer.style['transform'] = 'rotateZ( '+ -data.x +'deg )';
		}

		var onContainerMouseDown = function(event) {
			event.preventDefault();
			isUserInteracting = true;
			onPointerDownPointerX = event.clientX;
			onPointerDownPointerY = event.clientY;
			onPointerDownLon = lon;
			onPointerDownLat = lat;
		}

		var onContainerMouseMove = function(event) {
			if ( isUserInteracting ) {
				lon = ( onPointerDownPointerX - event.clientX ) * 0.1 + onPointerDownLon;
				lon = ( lon + 360 )%360;
				lat = ( event.clientY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;
				lat = Math.min( maxY, Math.max( lat, minY) );
				// Trigger move event
				triggerEvent('move', function(action) {
					action({
						x: lon,
						y: lat
					});
				});
			}
		}

		var onContainerMouseUp = function(event) {
			isUserInteracting = false;
		}

		var animate = function() {
			requestAnimationFrame( animate );
			render();
			console.log(123);
		}

		var triggerEvent = function(evName, execAction) {
			if(!eventRegisteredActions[evName] || !eventRegisteredActions[evName].length) return;
			eventRegisteredActions[evName].forEach(function(action) {
				execAction(action);
			})
		}

		var render = function() {
			renderer.clear();
			lat = Math.max( - 85, Math.min( 85, lat ) );
			phi = THREE.Math.degToRad( 90 - lat );
			theta = THREE.Math.degToRad( lon );
			camera.target.x = 500 * Math.sin( phi ) * Math.cos( theta );
			camera.target.y = 500 * Math.cos( phi );
			camera.target.z = 500 * Math.sin( phi ) * Math.sin( theta );
			camera.lookAt( camera.target );
			renderer.render( scene, camera );
		}

		self.on = function(evName, callback) {
			if(!eventRegisteredActions[evName]){
				eventRegisteredActions[evName] = [];
			}
			eventRegisteredActions[evName].push(callback);
		}

		self.on('move', function() {
			render();
		});

		self.moveSoftTo = function(coords, onEnd) {
			coords.x = ( (typeof coords.x!='undefined') ? coords.x : lon );
			coords.y = ( (typeof coords.y!='undefined') ? coords.y : lat );
			var nSteps = 35;
			var distanceX = coords.x-lon;
			var distanceY = coords.y-lat;
			if(!distanceX && !distanceY){
				onEnd();
				return;
			}
			var stepX = distanceX/nSteps;
			var stepY = distanceY/nSteps;
			var interval = setInterval(function() {				
				lon += stepX;
				lat += stepY;
				triggerEvent('move', function(action) {
					action({
						x: lon,
						y: lat
					});
				});
				nSteps--;
				if(!nSteps){
					clearInterval( interval );
					if(onEnd) {
						onEnd();
					}
				}
			},20);
		}

		self.moveTo = function(params) {
			if (!params) return;
			if (params.x) lon = params.x;
			if (params.y) lat = params.y;
			render();
			triggerEvent('move', function(action) {
				action({
					x: lon,
					y: lat
				});
			});

		}

		self.zoomIn = function(onEnd) {
			var frames = 25;
			var step = 1.4;
			var framesPerSecond = 40;
			var zoomInterval = setInterval(function() {
				fov -= step;
				camera.projectionMatrix.makePerspective( fov, containerDims.width / containerDims.height, 1, 1100 );
				render();
				frames--;
				if(!frames) {
					clearInterval( zoomInterval );
					if(onEnd) onEnd();
				}
			}, 1000/framesPerSecond );
		}

		self.zoomOut = function(onEnd) {
			var frames = 25;
			var step = 1.4;
			var framesPerSecond = 40;
			var zoomInterval = setInterval(function() {
				fov += step;
				camera.projectionMatrix.makePerspective( fov, containerDims.width / containerDims.height, 1, 1100 );
				render();
				frames--;
				if(!frames) {
					clearInterval( zoomInterval );
					if(onEnd) onEnd();
				}
			},1000/framesPerSecond);
		}

		self.resetZoom = function() {
			fov = 70;
			camera.projectionMatrix.makePerspective( fov, containerDims.width / containerDims.height, 1, 1100 );
			render();
		}

		self.updateImage = function(imgSrc) {
			setEquiviewImage(imgSrc);
			imageSource = imgSrc;
			targetSubContainer.innerHTML = '';
			var buffImg = new Image;
			buffImg.src = imgSrc;
			render();
			buffImg.onload = function() {
				render();
			}
		}


	// Construct
		basicSetup();
		if(imageSource) {
			setEquiviewImage(imageSource);
			// animate();
			render();
		}

}