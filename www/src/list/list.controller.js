(function(ang) {

	var app = ang.module('app');

	app.controller('listController', ['$scope', '$mdSidenav', '$state', '$resources', function( $scope, $mdSidenav, $state, $resources ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Methods
			$scope.methods.downloadBuildings = function () {
				$resources.Buildings.get().then(function (resp) {
					$scope.models.buildings = resp.data.results;
					console.log( $scope.models.buildings )
				});
			}
			$scope.methods.goToBuilding = function (bId) {
				// $scope.methods.newNotification('Cargando inmueble..');
				$state.go('building', { buildingId: bId });
			}

		// Init
			$scope.methods.downloadBuildings();

	}]);

})(angular)