(function(ang) {

	var app = ang.module('app');

	app.directive('appContainer', function() {
		return {
			templateUrl: 'src/app/main.html',
			controller: 'appContainer'
		};
	});

})(angular)