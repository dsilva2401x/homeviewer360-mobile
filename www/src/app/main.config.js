(function(ang) {

	var app = ang.module('app');

	app.config(['$mdThemingProvider', '$resourcesProvider', '$stateProvider', '$urlRouterProvider', function ($mdThemingProvider, $resourcesProvider, $stateProvider, $urlRouterProvider) {

		// Material theme
			$mdThemingProvider.theme('default').primaryPalette('orange', {
				'default': '900',
				'hue-1': '700',
				'hue-2': '500',
				'hue-3': '200',
			});

		// Resources
			$resourcesProvider.init({
				resources: {

					Me: {
						// route: '/api/me'
						route: 'demos/me.json'
					},

					Buildings: {
						// route: 'demos/buildings:buildingId.json'
						route: 'http://api.homeviewer360.com/api/v1/inmuebles/:buildingId/'
					}

				}
			})

		// Routes
			$urlRouterProvider.otherwise('/search');
			$stateProvider
				.state('search', {
					url: '/search',
					templateUrl: 'src/search/search.html',
					controller: 'searchController'
				})
				.state('list', {
					url: '/list',
					templateUrl: 'src/list/list.html',
					controller: 'listController'
				})
				.state('building', {
					url: '/building/:buildingId',
					templateUrl: 'src/building/building.html',
					controller: 'buildingController'
				})

	}]);

})(angular)