(function(ang) {

	var app = ang.module('app');

	app.controller('buildingController', ['$scope', '$mdSidenav', '$state', '$resources', 'downloadViewData', '$stateParams', '$http', function( $scope, $mdSidenav, $state, $resources, downloadViewData, $stateParams, $http ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Methods
			$scope.methods.loadBuilding = function (bId) {
				// $http.get('demos/building.json').success(function (bdata) {
					
				// })

				$resources.Buildings.get({
					urlParams: { buildingId: bId }
				}).then(function (resp) {
					$scope.models.building = resp.data;
					document.getElementById('eqcontainer').src = 'http://hv360-mobile.herokuapp.com?buildingId='+$scope.models.building.id;
					// console.log(resp);
					console.log('Loaded building: ', resp.data);
				});
			}
		// Init
			$scope.methods.loadBuilding( $stateParams.buildingId );


	}]);

})(angular)